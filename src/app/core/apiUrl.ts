export const ApiUrl = {

    LOGIN: 'Admin/Login',
    upload_attachment: 'common/v1/upload',
    TUTORIAL_LIST: 'Admin/tutorialListing',
    ADD_TUTORIAL: 'Admin/addEditToutorial',
    MANAGE_TUTORIAL: 'Admin/deleteBlockTutorial',

     CategoryList: 'Admin/categoryListing',
     ADD_CATEGORY: 'Admin/addEditCategory',
     MANAGE_CATEGORY: 'Admin/deleteCategory',

     Subscription: 'Admin/subscribePlansListing',
     ADD_SUBSCRIPTION: 'Admin/addEditSubscribePlans',
     MANAGE_SUBSCRIPTION: 'Admin/deleteSubscribePlans',

     LanguageListing: 'Admin/languageListing',
     ADD_LANGUAGE: 'Admin/addEditLanguage',
     MANAGE_LANGUAGE: 'Admin/deleteLanguage',

     PROFILE_LISTING: 'Admin/profileInfoListing',
     ADD_PROFILEINFO:'Admin/addEditProfileInfo',
     MANAGE_PROFILE:'Admin/deleteProfileInfo',

     Question_Listing: 'Admin/questionslisting',
     ADD_QUESTIONS: 'Admin/addEditQuestions',
     DELETE_QUESTIONS: 'Admin/deleteBlockQuestions',

     Answers_Listing: 'Admin/answerslisting',
     ADD_Answers: 'Admin/addEditAnswers',
     MANAGE_ANSWERS: 'Admin/deleteBlockAnswers',

     Cities_Listing: 'Admin/citiesListing',
     ADD_Cities: 'Admin/addEditCities',
     MANAGE_CITIES: 'Admin/deleteCities',

     Video_List: 'Admin/videosListing',
     ADD_VIDEO: 'Admin/addEditVideos',
     MANAGE_VIDEOS: 'Admin/deleteVideos',
 
    BANNER_LIST: 'Admin/bannersListing',
    ADD_BANNER: 'Admin/addEditBanners',
    MANAGE_BANNER: 'Admin/deleteBlockedBanners',

    studioListing:'Admin/studioListing',
    Add_Studio:'Admin/addEditStudio',
    MANAGE_STUDIO:'Admin/deleteBlockedStudio',
    
    ABOUT_LISTING:'Admin/aboutUsListing',
    ADD_ABOUT:'Admin/addEditAboutUs',
    MANAGE_ABOUT:'Admin/deleteBlockedAboutUs',

    USER_LIST: 'Admin/userListing',
    MANAGE_USER: 'Admin/blockUser',

   
    Plan_List: 'Admin/plansListing',
    ADD_PLAN: 'Admin/addEditPlans',
    MANAGE_PLAN:'Admin/deleteBlockedPlans',

    Session_List: 'Admin/sessionListing',
    ADD_Session: 'Admin/addEditSession',
    MANAGE_SESSION:'Admin/deleteBlockedSession',

    Article_Listing:'Admin/articalLsting',
    ADD_ARTICLE: 'Admin/addEditArticals',
    MANAGE_ARTICLE:'Admin/deleteBlockedAtrical',

    PolicyTermListing:'Admin/policyTermsListing',
    Add_policyTerm:'Admin/addEditPolicyTermsCondntion',
    MANAGE_POLICYTERM:'Admin/deleteBlockedPolicyTerms',

    Contact_Listing: 'Admin/aboutContact',
    ADD_CONTACT: 'Admin/contacts',
    MANAGE_CONTACT: 'Admin/deleteBlockedContact',

    MemberShip_Listing: 'Admin/memberShipPlanListing',
    ADD_MEMBER: 'Admin/addEditMemberShipPlan',
    MANAGE_MEMBERSHIP: 'Admin/deleteBlockedMemberShipPlan',

    Query_Listing:'Admin/queryListing',

    

};
