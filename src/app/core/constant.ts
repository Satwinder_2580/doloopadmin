export const GlobalVariable = {

    SITE_NAME: 'DoLoop Admin',
    PATTERNS: {
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    },
    tokenKey: 'DoLoop_token'

};

export const SideBarList = {
    list: [
        {path: 'tutorials', name: 'Tutorials', icon: 'fa fa-child'}, 
        // {path: 'categories', name: 'Category', icon: 'fa fa-linode'}, 
        // {path: 'subscription', name: 'Subscription', icon: 'fa fa-flag'},
        // {path: 'language', name: 'Language', icon: 'fa fa-key'},
        {path: 'users', name: 'Users', icon: 'fa fa-users'},
        // {path: 'profileinfo', name: 'Profile Info', icon: 'fa fa-picture-o'},
        {path: 'question', name: 'Questions', icon: 'fa fa-question-circle'},
        {path: 'answers', name: 'Answers', icon: 'fa fa-thumbs-up'},
        // {path: 'cities', name: 'Country', icon: 'fa fa-user'},
        // {path: 'videos', name: 'Videos', icon: 'fa fa-video-camera'},
        //   {path: 'plans', name: 'Plans', icon: 'fa fa-handshake-o'},
        //   {path: 'membership', name: 'MemberShip Plan',  icon: 'fa fa-linode'},
        //   {path: 'article', name: 'Articles', icon: 'fa fa-picture-o'},
        //   {path: 'video', name: 'Videos', icon: 'fa fa-video-camera'},
           {path: 'policy', name: 'Privacy Policy', icon: 'fa fa-user-secret'},
           {path: 'terms', name: 'Terms and Condition',  icon: 'fa fa-check-circle'},
        //    {path: 'contact', name: 'Contact us',  icon: 'fa fa-address-book-o'},
        //    {path: 'about', name: 'About us', icon: 'fa fa-handshake-o'},
        //    {path: 'query', name: 'Queries', icon: 'fa fa-thumbs-up'},

    ]
};

export const CONSTANT = {
    languages: [{name: 'English', id: 1}, {name: 'Spanish', id: 2}]
};

