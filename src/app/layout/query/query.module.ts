import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QueryComponent } from './query/query.component';
import {Routes,RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';

const routes: Routes=[
  {
    path: '',component:QueryComponent
  }
];

@NgModule({
  declarations: [QueryComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),


  ]
})
export class QueryModule { }
