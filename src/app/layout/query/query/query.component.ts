import { Component, OnInit } from '@angular/core';
import { PaginationControls } from 'src/app/shared/models/pagination-model';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { UtilService } from 'src/app/services/util/util.service';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { MessageService } from 'src/app/services/message/message.service';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.scss']
})
export class QueryComponent implements OnInit {
  allData: any = [];
  pagination = new PaginationControls();

  constructor(private http: HttpService, private message: MessageService, public util: UtilService,
              private modalService: BsModalService) {
  }
  ngOnInit(): void {
    this.getData();
  }
  getData() {
    const obj = {
        pageNumber: (this.pagination.page - 1) * 10
    };
    this.http.getData(ApiUrl.Query_Listing, obj).subscribe(res => {
        this.allData = res.data.dataList;
        this.pagination.count = res.data.totalCount;
    }, () => {
    });
}
}
