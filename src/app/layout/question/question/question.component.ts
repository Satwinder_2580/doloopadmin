import { Component, OnInit } from '@angular/core';
import { PaginationControls } from 'src/app/shared/models/pagination-model';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { UtilService } from 'src/app/services/util/util.service';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { MessageService } from 'src/app/services/message/message.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AddQuestionComponent } from '../add-question/add-question.component';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
  allData: any = [];
  pagination = new PaginationControls();
    allCategories: any;
    _id: any;

  constructor(private http: HttpService, private message: MessageService, public util: UtilService,
              private modalService: BsModalService) {
  }
  ngOnInit(): void {
    this.getData();
    // this.getCategoryList();
  }
  getData() {
    const obj = {
        pageNumber: (this.pagination.page - 1) * 10
    };
    this.http.getData(ApiUrl.Question_Listing,obj).subscribe(res => {
        this.allData = res.data.dataList;
        this.pagination.count = res.data.totalCount;
    }, () => {
    });
}

// blockUnblock(data) {
//     this.message.confirm(`${data.isBlocked ? 'unblock' : 'block'} this ${this.util.title}`).then(result => {
//         if (result.value) {
//             const obj: any = {
//                 _id: data._id,
//                 isBlocked: !data.isBlocked
//             };
//             this.http.putData(ApiUrl.DELETE_QUESTIONS,obj).subscribe(() => {
//                 this.util.checkBlockUnblock(data);
//             }, () => {
//             });
//         }
//     });
// }

deleteData(data,index) {
    this.message.confirm(`delete this ${this.util.title}`).then(result => {
        if (result.value) {
            const obj: any = {
                _id: data._id,
                isDeleted: true
            };
            this.http.putData(ApiUrl.DELETE_QUESTIONS,obj,true).subscribe(() => {
                this.message.toast('success', SuccessErrorConst.deleteSuccess);
                this.allData.splice(index,1);
            }, () => {
            });
        }
    });

}

addEditModalOpen(data?: any) {
    const modalRef = this.modalService.show(AddQuestionComponent, {
        initialState: {modalData: data}, backdrop: 'static', keyboard: false, class: 'modal-lg'
    });
    modalRef.content.onClose.subscribe(result => {
        this.getData();
    });
}
// getCategoryList() {
//     this.http.getData(ApiUrl.CategoryList,true).subscribe(res => {
//         this.allCategories = res.data.dataList;
//         this._id = res.data._id;
//     }, () => {
//     });
// }
// categoryId;

// oncategoryId(categoryId ?:any): void {
//     const obj: any = {
//         categoryId
//     };
//     this.http.getData(ApiUrl.Question_Listing, obj ).subscribe(res => {
//         this.allData = res.data.dataList;
//        console.log( this.allData)
//         this.pagination.count = res.data.totalCount;
//     });
          
// }

}
