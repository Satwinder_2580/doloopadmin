import { Component, OnInit } from '@angular/core';
import { PaginationControls } from 'src/app/shared/models/pagination-model';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { UtilService } from 'src/app/services/util/util.service';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { MessageService } from 'src/app/services/message/message.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AddarticleComponent } from '../addarticle/addarticle.component';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  allData: any = [];
  pagination = new PaginationControls();
  search: any;

  constructor(private http: HttpService, private message: MessageService, public util: UtilService,private modalService:BsModalService) {
  }

  ngOnInit() {
      this.getData();
  }

  getData() {
      const obj: any = {
          pageNumber: (this.pagination.page - 1) * 10
        };
        // if (this.search) {
        //   obj.search = this.search;
        // } else {
        //   delete obj.search;
        // }
      this.http.getData(ApiUrl.Article_Listing, obj).subscribe(res => {
          this.allData = res.data.dataList;
          this.pagination.count = res.data.totalCount;
      });
  }
  blockUnblock(data) {
    this.message.confirm(`${data.isBlocked ? 'unblock' : 'block'} this ${this.util.title}`).then(result => {
        if (result.value) {
            const obj: any = {
                _id: data._id,
                isBlocked: !data.isBlocked
            };
            this.http.putData(ApiUrl.MANAGE_ARTICLE,obj).subscribe(() => {
                this.util.checkBlockUnblock(data);
            }, () => {
            });
        }
    });
  }
  
  deleteData(data,index) {
    this.message.confirm(`delete this ${this.util.title}`).then(result => {
        if (result.value) {
            const obj: any = {
                _id: data._id,
                isDeleted: true
            };
            this.http.putData(ApiUrl.MANAGE_ARTICLE,obj,true).subscribe(() => {
                this.message.toast('success', SuccessErrorConst.deleteSuccess);
                this.allData.splice(index,1);
            }, () => {
            });
        }
    });
  
  }
  
  addEditModalOpen(data?: any) {
    const modalRef = this.modalService.show(AddarticleComponent, {
        initialState: {modalData: data}, backdrop: 'static', keyboard: false, class: 'modal-md'
    });
    modalRef.content.onClose.subscribe(result => {
        this.getData();
    });
  }
  
  }
