import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'src/app/services/message/message.service';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { UtilService } from '../../../services/util/util.service';

@Component({
  selector: 'app-addarticle',
  templateUrl: './addarticle.component.html',
  styleUrls: ['./addarticle.component.scss']
})
export class AddarticleComponent implements OnInit {
  showError = false;
  form: FormGroup;
  public onClose: Subject<{}> = new Subject();
  modalData: any;
  studioList: any;
  text: any;

  constructor(private fb: FormBuilder, private message: MessageService, private http: HttpService,
              public bsModalRef: BsModalRef, public util: UtilService
  ) {
  }

  ngOnInit() {
    this.makeForm();  
    this.studioid();
}

makeForm() {
  console.log(this.form)
    this.form = this.fb.group({
        studioId: ['', Validators.required],
        coverImage: ['',Validators.required],
        image: ['',Validators.required],
        title: ['', Validators.required],
        description: ['',Validators.required],
       text: [this.text],
    });
    if (this.modalData) {
      this.patchData(this.modalData);
    } else {
  
    }
  }
 
  patchData(data) {
  console.log(this.form)
    this.form.patchValue({
      studioId:data.studioId,
      coverImage:data.coverImage,
      image:data.image,
      title:data.title,
      text:data.text, 
      description:data.description

    });
  
      }
      formSubmit() {
        console.log(this.form.value)
        if (this.form.valid) {
          const obj = JSON.parse(JSON.stringify(this.form.value));
          // obj.steps = this.getValue(this.form.value.steps,'steps');
          // obj.passage = this.getValue(this.form.value.passage,'passage');
          // obj.text=this.form.value.text;
          // obj.text = this.text
          let msg = SuccessErrorConst.addedSuccess;
          if (this.modalData) {
            obj[`_id`] = this.modalData._id;
            msg = SuccessErrorConst.updatedSuccess;
          }
          let text = this.form.value;
         text['text'] = this.text

        //  alert(JSON.stringify(obj))
          this.http.postData(ApiUrl.ADD_ARTICLE, obj).subscribe(
            () => {
              this.onClose.next();
              this.message.toast('success', msg);
              this.bsModalRef.hide();
            }, () => {
            });
        // } else {
            // this.showError = true;
        // }
          }
      }

      selectcoverImage(event: any) {
        if (event.target.files && event.target.files[0]) {
            const obj = {
                image: event.target.files[0]
            };
            this.selectuploadImage(obj);
        }
    }
    
    selectuploadImage(obj) {
        this.http.uploadImageService(ApiUrl.upload_attachment, obj, true).subscribe(response => {
            this.form.controls.coverImage.setValue(this.util.setImagePath(response.data));
            document.getElementById('coverImage')[`value`] = '';
        }, () => {
            document.getElementById('coverImage')[`value`] = '';
        });
    }
    
    selectremoveImage() {
        this.form.controls[`coverImage`].setValue('');
    }

      selectImage(event: any) {
        if (event.target.files && event.target.files[0]) {
            const obj = {
                image: event.target.files[0]
            };
            this.uploadImage(obj);
        }
    }
    
    uploadImage(obj) {
        this.http.uploadImageService(ApiUrl.upload_attachment, obj, true).subscribe(response => {
            this.form.controls.image.setValue(this.util.setImagePath(response.data));
            document.getElementById('image')[`value`] = '';
        }, () => {
            document.getElementById('image')[`value`] = '';
        });
    }
    
    removeImage() {
        this.form.controls[`image`].setValue('');
    }

  studioid(){
  this.http.getData(ApiUrl.studioListing,{}, true).subscribe(res => {
    this.studioList = res.data.dataList;
}, () => {
});
}
}

