import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article/article.component';
import { AddarticleComponent } from './addarticle/addarticle.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
const routes: Routes = [
    {
        path: '', component: ArticleComponent
    }
];



@NgModule({
  declarations: [ArticleComponent, AddarticleComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    AngularEditorModule
],
entryComponents: [
    AddarticleComponent
]
})
export class ArticleModule { }
