import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'src/app/services/message/message.service';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { UtilService } from '../../../services/util/util.service';
@Component({
  selector: 'app-addplan',
  templateUrl: './addplan.component.html',
  styleUrls: ['./addplan.component.scss']
})
export class AddplanComponent implements OnInit {
  showError = false;
  form: FormGroup;
  public onClose: Subject<{}> = new Subject();
  modalData: any;
  allData: any;

  constructor(private fb: FormBuilder, private message: MessageService, private http: HttpService,
              public bsModalRef: BsModalRef, public util: UtilService
  ) {
  }

  ngOnInit() {
    this.makeForm();  
    this.studioid();
}

makeForm() {
    this.form = this.fb.group({
        studioId: ['', Validators.required],
        name: ['',Validators.required],
        numberOfClasses:['',Validators.required],
        price: ['', Validators.required],
        currency: ['', Validators.required],
        
    });
    if (this.modalData) {
        this.patchData(this.modalData);
    }
}

patchData(data) {
  console.log(data)
       this.form.patchValue({
        studioId: data.studioId,
        name: data.name,
        numberOfClasses: data.numberOfClasses,
        price:data.price,
        currency:data.currency,
        
    });
}
addEdit() {
  console.log(this.form.value)
    if (this.form.valid) {
        const obj = JSON.parse(JSON.stringify(this.form.value));
        let msg = SuccessErrorConst.addedSuccess;
        if (this.modalData) {
            obj[`_id`] = this.modalData._id;
            msg = SuccessErrorConst.updatedSuccess;
        }
        this.http.postData(ApiUrl.ADD_PLAN, obj).subscribe(() => {
            this.onClose.next();
            this.message.toast('success', msg);
            this.bsModalRef.hide();
        }, () => {
        });
    } else {
        this.showError = true;
    }
}
studioid(){
  this.http.getData(ApiUrl.studioListing,{}, true).subscribe(res => {
    this.allData = res.data.dataList;
}, () => {
});
} 
}
