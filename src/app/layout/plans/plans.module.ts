import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlansComponent } from './plans/plans.component';
import { AddplanComponent } from './addplan/addplan.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
    {
        path: '', component:PlansComponent
    }
];



@NgModule({
  declarations: [PlansComponent, AddplanComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
],
entryComponents: [
    AddplanComponent
]
})
export class PlansModule { }
