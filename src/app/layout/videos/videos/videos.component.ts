import { Component, OnInit } from '@angular/core';
import { PaginationControls } from 'src/app/shared/models/pagination-model';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { UtilService } from 'src/app/services/util/util.service';
import { MessageService } from 'src/app/services/message/message.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { AddVideosComponent } from '../add-videos/add-videos.component';
@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {
  allData: any = [];
  pagination = new PaginationControls();
  search: any;
    _id: any;
    allCategories: any;

  constructor(private http: HttpService, private message: MessageService, public util: UtilService,private modalService:BsModalService) {
  }

  ngOnInit() {
      this.getData();
      this.getCategoryList();
  }

  getData() {
      const obj: any = {
          pageNumber: (this.pagination.page - 1) * 10
        };
        // if (this.search) {
        //   obj.search = this.search;
        // } else {
        //   delete obj.search;
        // }
      this.http.getData(ApiUrl.Video_List, obj).subscribe(res => {
          this.allData = res.data.dataList;
          this.pagination.count = res.data.totalCount;
      });
  }
  
// blockUnblock(data) {
// this.message.confirm(`${data.isBlocked ? 'unblock' : 'block'} this ${this.util.title}`).then(result => {
//     if (result.value) {
//         const obj: any = {
//             id: data._id,
//             isBlocked: !data.isBlocked
//         };
//         this.http.putData(ApiUrl.MANAGE_VIDEOS, obj).subscribe(() => {
//             this.util.checkBlockUnblock(data);
//         }, () => {
//         });
//     }
// });
// }

deleteData(data,index) {
this.message.confirm(`delete this ${this.util.title}`).then(result => {
    if (result.value) {
        const obj: any = {
            id: data._id,
            isDeleted: true
        };
        this.http.putData(ApiUrl.MANAGE_VIDEOS, obj).subscribe(() => {
            this.message.toast('success', SuccessErrorConst.deleteSuccess);
            this.allData.splice(index,1);
        }, () => {
        });
    }
});

}
  addEditModalOpen(data?: any) {
    const modalRef = this.modalService.show(AddVideosComponent, {
        initialState: {modalData: data}, backdrop: 'static', keyboard: false, class: 'modal-more-lg'
    });
    modalRef.content.onClose.subscribe(() => {
        this.getData();
    });
}

getCategoryList() {
    this.http.getData(ApiUrl.CategoryList,true).subscribe(res => {
        this.allCategories = res.data.dataList;
        this._id = res.data._id;
    }, () => {
    });
}
  

categoryId;

oncategoryId(categoryId ?:any): void {
    const obj: any = {
        categoryId
    };
    // if (this.search) {
    //     obj.search = this.search;
    // } else {
    //     delete obj.search;
    // }
    this.http.getData(ApiUrl.Video_List, obj ).subscribe(res => {
        this.allData = res.data.dataList;
       console.log( this.allData)
        this.pagination.count = res.data.totalCount;
    });
          
}


}

