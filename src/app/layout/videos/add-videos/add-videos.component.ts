import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'src/app/services/message/message.service';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { UtilService } from '../../../services/util/util.service';

@Component({
    selector: 'app-add-videos',
    templateUrl: './add-videos.component.html',
    styleUrls: ['./add-videos.component.scss']
})
export class AddVideosComponent implements OnInit {
    showError = false;
    form: FormGroup;
    public onClose: Subject<{}> = new Subject();
    modalData: any;
    allCategories: any;
    allQuestions: any;
    allAnswers: any;

    dropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 5,
        allowSearchFilter: true
    };
    constructor(private fb: FormBuilder, private message: MessageService, private http: HttpService,
                public bsModalRef: BsModalRef, public util: UtilService) {
    }

    ngOnInit(): void {
        this.makeForm();
        this.getCategory();
    }

    makeForm() {
        this.form = this.fb.group({
            categoryId: ['', Validators.required],
            videoUrl: ['', Validators.required],
            data: this.fb.array([]),
            thumbnail: ['', Validators.required],
            description: ['', Validators.required]
        });

        if (this.modalData) {
            this.patchData(this.modalData);
        } else {

            this.createItem();
            this.addItem();

        }
    }

    createItem(): FormGroup {
        return this.fb.group({
            questionId: ['', Validators.required],
            answerId: ['', Validators.required]
        });
    }

    addItem(): void {
        this.data.push(this.createItem());
    }

    get data(): FormArray {
        return this.form.get('data') as FormArray;
    }

    patchData(data1) {
        this.form.patchValue({
            categoryId: data1.categoryId,
            videoUrl: data1.videoUrl,
            thumbnail: data1.thumbnail,
            description: data1.description
        });
        this.data.removeAt(0);
        const data = [];
        data1.data.map((val) => {
            console.log(val, 'valvalval');
            this.addItem();
            data.push({
                questionId: val.questionId._id,
                answerId: val.answerId
            });
        });

        debugger;
        this.form.patchValue({
            data: data
        });
    }

    // getValue(arr,key){
    //   let temp=[];
    //   arr.forEach(element => {
    //       temp.push(element[key])
    //   });

    //   return temp;
    //   }
    addEdit() {
        console.log(this.form.value);
        if (this.form.valid) {
            const obj = JSON.parse(JSON.stringify(this.form.value));

            obj.data.map(x => {
                x.answerId = x.answerId.map(y => y._id);
            });

            let msg = SuccessErrorConst.addedSuccess;
            if (this.modalData) {
                obj[`_id`] = this.modalData._id;
                msg = SuccessErrorConst.updatedSuccess;
            }
            this.http.postData(ApiUrl.ADD_VIDEO, obj).subscribe(
                    () => {
                        this.onClose.next();
                        this.message.toast('success', msg);
                        this.bsModalRef.hide();
                    }, () => {
                    });
        } else {
            this.showError = true;
            // }
        }
    }

    getCategory() {
        this.http.getData(ApiUrl.CategoryList, true).subscribe(res => {
            this.allCategories = res.data.dataList;

            if (this.modalData) {
                this.getAnswers();
                this.getQuestions();
            }
        }, () => {
        });
    }

    selectImage(event: any) {
        if (event.target.files && event.target.files[0]) {
            const obj = {
                image: event.target.files[0]
            };
            this.uploadImage(obj);
        }
    }

    uploadImage(obj) {
        this.http.uploadImageService(ApiUrl.upload_attachment, obj, true).subscribe(response => {
            this.form.controls.thumbnail.setValue(this.util.setImagePath(response.data));
            document.getElementById('thumbnail')[`value`] = '';
        }, () => {
            document.getElementById('thumbnail')[`value`] = '';
        });
    }

    removeImage() {
        this.form.controls[`thumbnail`].setValue('');
    }

    getQuestions() {
        this.http.getData(ApiUrl.Question_Listing, {categoryId: this.form.value.categoryId}).subscribe(res => {
            this.allQuestions = res.data.dataList;
            console.log(this.allQuestions, 'this.allQuestions this.allQuestions this.allQuestions ');

        }, () => {
        });
    }

    getAnswers() {
        this.http.getData(ApiUrl.Answers_Listing, {categoryId: this.form.value.categoryId}).subscribe(res => {
            this.allAnswers = res.data.dataList;
            console.log(this.allAnswers, 'this.allAnswers');
        }, () => {
        });
    }

    changecategory() {
        this.getAnswers();
        this.getQuestions();
    }
}


