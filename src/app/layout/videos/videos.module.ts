import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideosComponent } from './videos/videos.component';
import { AddVideosComponent } from './add-videos/add-videos.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
    {
        path: '', component: VideosComponent
    }
];

@NgModule({
  declarations: [VideosComponent, AddVideosComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  entryComponents:[
     AddVideosComponent
  ]
})
export class VideosModule { }
