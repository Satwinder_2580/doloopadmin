import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LanguageComponent } from './language/language.component';
import { AddLanguageComponent } from './add-language/add-language.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';


const routes: Routes = [
  {
      path: '', component: LanguageComponent
  }
];


@NgModule({
  declarations: [LanguageComponent, AddLanguageComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  entryComponents:[
    AddLanguageComponent
  ]
})
export class LanguageModule { }
