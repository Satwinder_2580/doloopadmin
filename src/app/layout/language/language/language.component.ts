import { Component, OnInit } from '@angular/core';
import { PaginationControls } from 'src/app/shared/models/pagination-model';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { UtilService } from 'src/app/services/util/util.service';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { MessageService } from 'src/app/services/message/message.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AddLanguageComponent } from '../add-language/add-language.component';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss']
})
export class LanguageComponent implements OnInit {
  allData: any = [];
  pagination = new PaginationControls();

  constructor(private http: HttpService, private message: MessageService, public util: UtilService,
              private modalService: BsModalService) {
  }
  ngOnInit(): void {
    this.getData();
  }
  getData() {
    const obj = {
        pageNumber: (this.pagination.page - 1) * 10
    };
    this.http.getData(ApiUrl.LanguageListing,obj).subscribe(res => {
        this.allData = res.data.dataList;
        this.pagination.count = res.data.totalCount;
    }, () => {
    });
}

// blockUnblock(data) {
//     this.message.confirm(`${data.isBlocked ? 'unblock' : 'block'} this ${this.util.title}`).then(result => {
//         if (result.value) {
//             const obj: any = {
//                 _id: data._id,
//                 isBlocked: !data.isBlocked
//             };
//             this.http.putData(ApiUrl.MANAGE_LANGUAGE,obj).subscribe(() => {
//                 this.util.checkBlockUnblock(data);
//             }, () => {
//             });
//         }
//     });
// }

deleteData(data,index) {
    this.message.confirm(`delete this ${this.util.title}`).then(result => {
        if (result.value) {
            const obj: any = {
                _id: data._id,
                isDeleted: true
            };
            this.http.putData(ApiUrl.MANAGE_LANGUAGE,obj,true).subscribe(() => {
                this.message.toast('success', SuccessErrorConst.deleteSuccess);
                this.allData.splice(index,1);
            }, () => {
            });
        }
    });

}

addEditModalOpen(data?: any) {
    const modalRef = this.modalService.show(AddLanguageComponent, {
        initialState: {modalData: data}, backdrop: 'static', keyboard: false, class: 'modal-lg'
    });
    modalRef.content.onClose.subscribe(result => {
        this.getData();
    });
}

}


