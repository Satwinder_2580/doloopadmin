import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact/contact.component';
import { AddcontactComponent } from './addcontact/addcontact.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
      path: '', component: ContactComponent
  }
];

@NgModule({
  declarations: [ContactComponent, AddcontactComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
],

entryComponents:[
  AddcontactComponent
]
})
export class ContactModule { }
