import { Component, OnInit } from '@angular/core';
import { PaginationControls } from 'src/app/shared/models/pagination-model';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { UtilService } from 'src/app/services/util/util.service';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { MessageService } from 'src/app/services/message/message.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import {AddcontactComponent} from './../addcontact/addcontact.component'
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  allData: any = [];
  pagination = new PaginationControls();

  search='';
  constructor(private http: HttpService, private message: MessageService, public util: UtilService,
              private modalService: BsModalService) {
  }

  ngOnInit() {
    this.getData();
}
getData() {
  const obj: any = {
      pageNumber: (this.pagination.page - 1) * 10
      // pageNumber: (this.pagination.page)
  };
  if (this.search) {
      obj.search = this.search;
  } else {
      delete obj.search;
  }

  this.http.getData(ApiUrl.Contact_Listing, obj).subscribe(res => {
      this.allData = res.data.dataList;
      this.pagination.count = res.data.totalCount;
  }, () => {
  });
}
blockUnblock(data) {
  this.message.confirm(`${data.isBlocked ? 'unblock' : 'block'} this ${this.util.title}`).then(result => {
      if (result.value) {
          const obj: any = {
              id: data._id,
              isBlocked: !data.isBlocked
          };
          this.http.putData(ApiUrl.MANAGE_CONTACT, obj).subscribe(() => {
              this.util.checkBlockUnblock(data);
          }, () => {
          });
      }
  });
}

deleteData(data) {
  this.message.confirm(`delete this ${this.util.title}`).then(result => {
      if (result.value) {
          const obj: any = {
              id: data._id,
              isDeleted: true
          };
          this.http.putData(ApiUrl.MANAGE_CONTACT, obj).subscribe(() => {
              this.message.toast('success', SuccessErrorConst.deleteSuccess);
              this.getData();
          }, () => {
          });
      }
  });

}
// changeTime(time: any) {
//   var hours = Math.floor(time / 60);
//   var minutes = time % 60;
//   return hours + ':' + minutes;
// }

formatAMPM(time) {
    var hours = Math.floor(time / 60);
        var minutes:any = time % 60;
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

addEditModalOpen(data?: any) {
  const modalRef = this.modalService.show(AddcontactComponent, {
      initialState: {modalData: data}, backdrop: 'static', keyboard: false, class: 'modal-md'
  });
  modalRef.content.onClose.subscribe(result => {
      this.getData();
  });
}

}