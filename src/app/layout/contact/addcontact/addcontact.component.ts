import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { MessageService } from "src/app/services/message/message.service";
import { ApiUrl } from "src/app/core/apiUrl";
import { HttpService } from "src/app/services/http/http.service";
import { Subject } from "rxjs";
import { BsModalRef } from "ngx-bootstrap/modal";
import { SuccessErrorConst } from "src/app/core/successErrorConst";
import { UtilService } from "../../../services/util/util.service";
import * as moment from 'moment';
@Component({
  selector: 'app-addcontact',
  templateUrl: './addcontact.component.html',
  styleUrls: ['./addcontact.component.scss']
})
export class AddcontactComponent implements OnInit {
  showError = false;
  form: FormGroup;
  public onClose: Subject<{}> = new Subject();
  modalData: any;
  

  constructor(
    private fb: FormBuilder,
    private message: MessageService,
    private http: HttpService,
    public bsModalRef: BsModalRef,
    public util: UtilService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      mobileNumber: ['', Validators.required],
      countryCode: ['',Validators.required],
      callStartTime: [new Date(), Validators.required],
      callEndTime: [new Date(), Validators.required],
      email: ['',Validators.required],
      instagramUrl: ['',Validators.required]

    });

    if (this.modalData) {
      this.patchData(this.modalData);
    } else {
      // this.createItem("longDiscription");
      // this.addItem("longDiscription");
    }
  }
  patchData(data) {
    console.log(data)
    this.form.patchValue({
      mobileNumber:data.mobileNumber,
      countryCode:data.countryCode,
      callStartTime:new Date(new Date().setHours(0,data.callStartTime)),
      callEndTime:new Date(new Date().setHours(0,data.callEndTime)),
      email:data.email,
      instagramUrl:data.instagramUrl,
    });

  //   const longDiscription = [];
  //   data.longDiscription.map((val) => {
  //     this.addItem("longDiscription");
  //     longDiscription.push({
  //       longDiscription: val.longDiscription,
  //     });
  //   });
  //   this.form.patchValue({
  //     longDiscription,
  //   });
  // }
  // getValue(arr,key){
  //   console.log(arr,'opoopo')
  //   let temp=[];
  //   arr.forEach(element => {
  //       temp.push(element[key])
  //   });
    
  //   return temp;
    
    }
  formSubmit() {
    if (this.form.valid) {
      const obj = JSON.parse(JSON.stringify(this.form.value));
      obj.callStartTime = moment(obj.callStartTime).format("HH:mm");
      obj.callEndTime = moment(obj.callEndTime).format("HH:mm");
      let msg = SuccessErrorConst.addedSuccess;
      if (this.modalData) {
        obj[`_id`] = this.modalData._id;
        msg = SuccessErrorConst.updatedSuccess;
      }
      this.http.postData(ApiUrl.ADD_CONTACT, obj).subscribe(
        () => {
          this.onClose.next();
          this.message.toast('success', msg);
          this.bsModalRef.hide();
        },
        () => {}
      );
    } else {
      this.showError = true;
    }
  }
}
 
