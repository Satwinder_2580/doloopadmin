import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileinfoComponent } from './profileinfo/profileinfo.component';
import { AddprofileinfoComponent } from './profileinfo/addprofileinfo/addprofileinfo.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
    {
        path: '', component: ProfileinfoComponent
    }
];

@NgModule({
    declarations: [ProfileinfoComponent, AddprofileinfoComponent],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    entryComponents: [
        AddprofileinfoComponent,
    ]
})
export class ProfileinfoModule {
}
