import { Component, OnInit } from '@angular/core';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { MessageService } from 'src/app/services/message/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

 
  text = '';
  type = '';
  update = false;
  allData: any = false;
  terms ='';

  constructor(private http: HttpService, private message: MessageService, private router: Router) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.http.getData(ApiUrl.PolicyTermListing, {}).subscribe(res => {
            if (res.data.length) {
                this.update = true;
                this.allData = res.data[0].terms[0];
                this.terms = res.data[0].terms;
            } else {
                this.allData = undefined;
            }
        }, () => {
        });
    }
    submitForm() {
          const obj: any = {};
          if (this.update) {
              obj._id = this.allData._id;
          }
          obj.terms=this.terms
    
          this.http.postData(ApiUrl.Add_policyTerm, obj).subscribe(() => {
              this.message.toast('success', SuccessErrorConst.updatedSuccess);
          }, () => {
          });
      } 
  }

