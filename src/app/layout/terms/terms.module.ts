import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TermsComponent } from './terms/terms.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { TrimValueAccessorModule } from 'ng-trim-value-accessor';
import { AngularEditorModule } from '@kolkov/angular-editor';


const routes: Routes = [
  {
    path: '', component: TermsComponent
  }
];

@NgModule({
  declarations: [TermsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    TrimValueAccessorModule,
    AngularEditorModule
  ]
})
export class TermsModule { }
