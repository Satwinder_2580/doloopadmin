import { Component, OnInit } from '@angular/core';
import { PaginationControls } from 'src/app/shared/models/pagination-model';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { UtilService } from 'src/app/services/util/util.service';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { MessageService } from 'src/app/services/message/message.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AddTutorialComponent } from './add-tutorial/add-tutorial.component';

@Component({
    selector: 'app-tutorials',
    templateUrl: './tutorials.component.html'
})
export class TutorialsComponent implements OnInit {

    allData: any = [];
    pagination = new PaginationControls();

    constructor(private http: HttpService, private message: MessageService, public util: UtilService,
                private modalService: BsModalService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        // const obj = {
        //     pageNumber: (this.pagination.page - 1) * 10
        // };
        this.http.getData(ApiUrl.TUTORIAL_LIST).subscribe(res => {
            this.allData = res.data;
            // this.pagination.count = res.data.totalCount;
        }, () => {
        });
    }

    blockUnblock(data) {
        this.message.confirm(`${data.isBlocked ? 'unblock' : 'block'} this ${this.util.title}`).then(result => {
            if (result.value) {
                const obj: any = {
                    _id: data._id,
                    isBlocked: !data.isBlocked
                };
                this.http.putData(ApiUrl.MANAGE_TUTORIAL + '?_id=' + data._id + '&isBlocked=' + !data.isBlocked, obj).subscribe(() => {
                    this.util.checkBlockUnblock(data);
                }, () => {
                });
            }
        });
    }

    deleteData(data) {
        this.message.confirm(`delete this ${this.util.title}`).then(result => {
            if (result.value) {
                const obj: any = {
                    _id: data._id,
                    isDeleted: true
                };
                this.http.putData(ApiUrl.MANAGE_TUTORIAL + '?_id=' + data._id + '&isDeleted=' + true, obj).subscribe(() => {
                    this.message.toast('success', SuccessErrorConst.deleteSuccess);
                    this.getData();
                }, () => {
                });
            }
        });

    }

    addEditModalOpen(data?: any) {
        const modalRef = this.modalService.show(AddTutorialComponent, {
            initialState: {modalData: data}, backdrop: 'static', keyboard: false, class: 'modal-more-lg'
        });
        modalRef.content.onClose.subscribe(() => {
            this.getData();
        });
    }

}
