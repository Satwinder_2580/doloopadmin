import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TutorialsComponent } from './tutorials.component';
import { AddTutorialComponent } from './add-tutorial/add-tutorial.component';

import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
    {
        path: '', component: TutorialsComponent
    }
];

@NgModule({
    declarations: [
        TutorialsComponent,
        AddTutorialComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    entryComponents: [
        AddTutorialComponent
    ]
})
export class TutorialsModule {
}
