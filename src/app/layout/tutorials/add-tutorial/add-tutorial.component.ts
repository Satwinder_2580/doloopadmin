import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'src/app/services/message/message.service';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { UtilService } from '../../../services/util/util.service';

@Component({
    selector: 'app-add-tutorial',
    templateUrl: './add-tutorial.component.html'
})
export class AddTutorialComponent implements OnInit {

    showError = false;
    form: FormGroup;
    public onClose: Subject<{}> = new Subject();
    modalData: any;

    constructor(private fb: FormBuilder, private message: MessageService, private http: HttpService,
                public bsModalRef: BsModalRef, public util: UtilService
    ) {
    }

    ngOnInit() {
        this.makeForm();    
    }

    makeForm() {
        this.form = this.fb.group({
            // image: ['', Validators.required],
            message: ['', Validators.required],
            // shortDiscription: ['', Validators.required],
            
        });
        if (this.modalData) {
            this.patchData(this.modalData);
        }
    }

    patchData(data) {

        this.form.patchValue({
            // image: data.image,
            message: data.message,
            // shortDiscription:data.shortDiscription
           
        });
    }
    addEdit() {
        if (this.form.valid) {
            const obj = JSON.parse(JSON.stringify(this.form.value));
            let msg = SuccessErrorConst.addedSuccess;
            if (this.modalData) {
                obj[`_id`] = this.modalData._id;
                msg = SuccessErrorConst.updatedSuccess;
            }
            this.http.postData(ApiUrl.ADD_TUTORIAL, obj).subscribe(() => {
                this.onClose.next();
                this.message.toast('success', msg);
                this.bsModalRef.hide();
            }, () => {
            });
        } else {
            this.showError = true;
        }
    }

    selectImage(event: any) {
        if (event.target.files && event.target.files[0]) {
            const obj = {
                image: event.target.files[0]
            };
            this.uploadImage(obj);
        }
    }

    uploadImage(obj) {
        this.http.uploadImageService(ApiUrl.upload_attachment, obj, true).subscribe(response => {
            this.form.controls.image.setValue(this.util.setImagePath(response.data));
            document.getElementById('image')[`value`] = '';
        }, () => {
            document.getElementById('image')[`value`] = '';
        });
    }

    removeImage() {
        this.form.controls[`image`].setValue('');
    }

}


