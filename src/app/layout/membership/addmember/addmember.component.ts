import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { MessageService } from "src/app/services/message/message.service";
import { ApiUrl } from "src/app/core/apiUrl";
import { HttpService } from "src/app/services/http/http.service";
import { Subject } from "rxjs";
import { BsModalRef } from "ngx-bootstrap/modal";
import { SuccessErrorConst } from "src/app/core/successErrorConst";
import { UtilService } from "../../../services/util/util.service"
import * as moment from 'moment';
@Component({
  selector: 'app-addmember',
  templateUrl: './addmember.component.html',
  styleUrls: ['./addmember.component.scss']
})
export class AddmemberComponent implements OnInit {

  showError = false;
  form: FormGroup;
  public onClose: Subject<{}> = new Subject();
  modalData: any;
  studioList: any;
  
  constructor(
    private fb: FormBuilder,
    private message: MessageService,
    private http: HttpService,
    public bsModalRef: BsModalRef,
    public util: UtilService
  ) {}

  ngOnInit(): void {   
    this.makeForm();
    this.studioid();
} 
 makeForm() {
    this.form = this.fb.group({
      studioId:['',Validators.required],
      name: ['',Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      title: ['',Validators.required],
      dates: ['', Validators.required],
      rules: this.fb.array([]),
      
    });
  
  if (this.modalData) {
    this.patchData(this.modalData);
  } else {

    this.createItem('rules');
    this.addItem('rules');
   
  }
}
createItem(type): FormGroup {
  switch (type) {
    case 'rules':
      return this.fb.group({
        rules: ['', Validators.required],
      });  
  }
}
addItem(type): void {
  switch (type) {
    case 'rules':
      this.rules.push(this.createItem(type));
      break;
  }
}

get rules(): FormArray {
  return this.form.get('rules') as FormArray;
}

patchData(data) {
  this.form.patchValue({
    studioId:data.studioId,
    name:data.name,
    currency:data.currency,
    price:data.price,
    title:data.title,
    dates: [new Date(parseInt(data.startDate, 10)), new Date(parseInt(data.endDate, 10))],
  });

  const rules = [];
    data.rules.map((val) => {
      this.addItem('rules');
      rules.push({
        rules: val
      });
    });

    this.form.patchValue({
      rules,
    });
  }
  getValue(arr,key){
    let temp=[];
    arr.forEach(element => {
        temp.push(element[key])
    });
    
    return temp;
    }
    formSubmit() {
      console.log(this.form.value)
      if (this.form.valid) {
        const obj = JSON.parse(JSON.stringify(this.form.value));
        obj.rules = this.getValue(this.form.value.rules,'rules');
        // if (this.image) {   
        //     obj.image = (this.image);
        // } else {
        //     this.message.toast('error', SuccessErrorConst.noImageAdded);
        //     return;
        // }
        const data: any = obj.dates;
        obj.startDate = moment(data[0]).format('YYYY-MM-DD');
        obj.endDate = moment(data[1]).format('YYYY-MM-DD');

       
        let msg = SuccessErrorConst.addedSuccess;
        if (this.modalData) {
          obj[`_id`] = this.modalData._id;
          msg = SuccessErrorConst.updatedSuccess;
        }
        delete obj.dates;
        this.http.postData(ApiUrl.ADD_MEMBER, obj).subscribe(
          () => {
            this.onClose.next();
            this.message.toast('success', msg);
            this.bsModalRef.hide();
          }, () => {
          });
      // } else {
          // this.showError = true;
      // }
        }
      }
      studioid(){
        this.http.getData(ApiUrl.studioListing,{}, true).subscribe(res => {
          this.studioList = res.data.dataList;
      }, () => {
      });
      }
      
    }
