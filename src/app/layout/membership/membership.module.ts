import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MembershipComponent } from './membership/membership.component';
import { AddmemberComponent } from './addmember/addmember.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';


const routes: Routes = [
  {
      path: '', component: MembershipComponent
  }
];

@NgModule({
  declarations: [MembershipComponent, AddmemberComponent],
  imports: [
    CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    entryComponents: [
      AddmemberComponent
  ]
})
export class MembershipModule { }
