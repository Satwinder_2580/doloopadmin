import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'src/app/services/message/message.service';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { UtilService } from '../../../services/util/util.service';
import * as moment from 'moment';

@Component({
    selector: 'app-addsession',
    templateUrl: './addsession.component.html',
    styleUrls: ['./addsession.component.scss']
})
export class AddsessionComponent implements OnInit {

    showError = false;
    form: FormGroup;
    public onClose: Subject<{}> = new Subject();
    modalData: any;
    allData: any;
    sessionList: any;
    studioList: any;
    nowTime = new Date();
    allPlan: any;

    limitSelection = false;
    ShowFilter = false;
    hstep = 1;
    mstep = 15;
    sstep = 10;

    options: any = {
        hstep: [1, 2, 3],
        mstep: [1, 5, 10, 15, 25, 30],
        sstep: [5, 10, 20, 30]
    };

    dropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 5,
        allowSearchFilter: true
    };
    minDate: Date;
    maxDate: Date;
    editData: any;

    constructor(private fb: FormBuilder, private message: MessageService, private http: HttpService,
                public bsModalRef: BsModalRef, public util: UtilService
    ) {
        this.minDate = new Date();
        this.maxDate = new Date();
        this.minDate.setDate(this.minDate.getDate());
        this.maxDate.setDate(this.maxDate.getDate() + 365);
    }

    ngOnInit() {
        this.makeForm();

        this.studioId();
    }

    makeForm() {
        this.form = this.fb.group({
            studioId: ['', Validators.required],
            planId: ['', Validators.required],
            image: ['', Validators.required],
            name: ['', Validators.required],
            date: [new Date(), Validators.required],
            startTime: [new Date(), Validators.required],
            durationTime: ['', Validators.required],
            discription: ['', Validators.required]
        });
        if (this.modalData) {
            this.patchData(this.modalData);
            console.log(this.form);
        }
    }

    studioClick(data) {
        console.log(data, 'studioClickstudioClickstudioClickstudioClick');
        this.planid(data._id);
    }

    createItem(type): FormGroup {
        switch (type) {
            case 'planId':
                return this.fb.group({
                    planId: ['', Validators.required]
                });
        }
    }

    addItem(type): void {
        switch (type) {
            case 'planId':
                this.planId.push(this.createItem(type));
                break;
        }
    }

    get planId(): FormArray {
        return this.form.get('planId') as FormArray;
    }

    patchData(data) {
        console.log(data);
        this.editData = data;
        this.form.patchValue({
            studioId: data.studioId,
            image: data.image,
            name: data.name,
            date: new Date(parseInt(data.date)),
            startTime: new Date(new Date().setHours(0, data.startTime)),
            durationTime: data.durationTime.toString(),
            discription: data.discription
        });

        this.planid(data.studioId, data.planId);

    }

    addEdit() {
        console.log(this.form);
        if (this.form.valid) {
            const obj = JSON.parse(JSON.stringify(this.form.value));
            const data: any = obj.date;
            obj.date = moment(data).format('YYYY-MM-DD');
            obj.startTime = moment(obj.startTime).format('HH:mm');
            // obj.durationTime =moment().format("HH:mm");
            let tempId: any = [];

            this.form.value.planId.forEach((val) => {
                tempId.push(val._id);
            });

            obj.planId = (tempId);
            let msg = SuccessErrorConst.addedSuccess;
            if (this.modalData) {
                obj[`_id`] = this.modalData._id;
                msg = SuccessErrorConst.updatedSuccess;
            }
            delete obj.delete;
            this.http.postData(ApiUrl.ADD_Session, obj).subscribe(() => {
                this.onClose.next();
                this.message.toast('success', msg);
                this.bsModalRef.hide();
            }, () => {
            });
        } else {
            this.showError = true;
        }
    }

    selectImage(event: any) {
        if (event.target.files && event.target.files[0]) {
            const obj = {
                image: event.target.files[0]
            };
            this.uploadImage(obj);
        }
    }

    uploadImage(obj) {
        this.http.uploadImageService(ApiUrl.upload_attachment, obj, true).subscribe(response => {
            this.form.controls.image.setValue(this.util.setImagePath(response.data));
            document.getElementById('image')[`value`] = '';
        }, () => {
            document.getElementById('image')[`value`] = '';
        });
    }

    removeImage() {
        this.form.controls[`image`].setValue('');
    }

    planid(id, editData?) {

        this.form.controls.planId.patchValue('');
        this.http.getData(ApiUrl.Plan_List, {studioId: id}, true).subscribe(res => {
            this.allPlan = res.data.dataList;

            if (this.editData) {
                let selected = this.editData.planId;
                // console.log(selected, 'selectedselectedselected', this.allPlan);
                let tempArr = [];
                this.allPlan.map(value => {
                    if (selected.includes(value._id)) {
                        tempArr.push(value);
                    }
                });
                this.form.patchValue({
                    planId: tempArr
                });
            }

        }, () => {
        });
    }

    studioId() {
        this.http.getData(ApiUrl.studioListing, {}, true).subscribe(res => {
            this.studioList = res.data.dataList;
        }, () => {
        });
    }
}

