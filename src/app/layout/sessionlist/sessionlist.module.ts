import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionlistComponent } from './sessionlist/sessionlist.component';
import { AddsessionComponent } from './addsession/addsession.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
      path: '', component: SessionlistComponent
  }
];
@NgModule({
  declarations: [SessionlistComponent, AddsessionComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
],
entryComponents:[
  AddsessionComponent
]
})
export class SessionlistModule { }
