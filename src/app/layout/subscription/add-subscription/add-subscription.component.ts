import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray,FormControl } from "@angular/forms";
import { MessageService } from "src/app/services/message/message.service";
import { ApiUrl } from "src/app/core/apiUrl";
import { HttpService } from "src/app/services/http/http.service";
import { Subject } from "rxjs";
import { BsModalRef } from "ngx-bootstrap/modal";
import { SuccessErrorConst } from "src/app/core/successErrorConst";
import { UtilService } from "../../../services/util/util.service"

@Component({
  selector: 'app-add-subscription',
  templateUrl: './add-subscription.component.html',
  styleUrls: ['./add-subscription.component.scss']
})
export class AddSubscriptionComponent implements OnInit {
  showError = false;
  form: FormGroup;
  public onClose: Subject<{}> = new Subject();
  modalData: any;
  
  constructor(
    private fb: FormBuilder,
    private message: MessageService,
    private http: HttpService,
    public bsModalRef: BsModalRef,
    public util: UtilService
  ) {}

  ngOnInit(): void {   
    this.makeForm();
} 
 makeForm() {
    this.form = this.fb.group({
      type:['',Validators.required],
      price:['',Validators.required],
      currency:['',Validators.required],
    });
  
  if (this.modalData) {
    this.patchData(this.modalData);
  } else {

    // this.createItem('value');
    // this.addItem('value');
   
  }
}
// createItem(type): FormGroup {
//   switch (type) {
//     case 'value':
//       return this.fb.group({
//         value: ['', Validators.required],
//       });  
//   }
// }
// addItem(type): void {
//   switch (type) {
//     case 'value':
//       this.value.push(this.createItem(type));
//       break;
//   }
// }

// get value(): FormArray {
//   return this.form.get('value') as FormArray;
// }

patchData(data) {
  console.log(data)
  this.form.patchValue({
    type:data.type,
    price:data.price,
    currency:data.currency,
  });

//   const value = [];
//   console.log(value)
//     data.value.forEach((val) => {
//       this.addItem('value');
//       value.push({
//         value: val
        
//       });
//     });
// console.log(value)
//     this.form.patchValue({
//       value,
//     });
//   }
//   getValue(arr,key){
//     let temp=[];
//     arr.forEach(element => {
//         temp.push(element[key])
//     });
    
//     return temp;
    
    }
    formSubmit() {
      if (this.form.valid) {
        const obj = JSON.parse(JSON.stringify(this.form.value));
        // if (this.image) {   
        //     obj.image = (this.image);
        // } else {
        //     this.message.toast('error', SuccessErrorConst.noImageAdded);
        //     return;
        // }

        // obj.value = this.getValue(this.form.value.value,'value');
        let msg = SuccessErrorConst.addedSuccess;
        if (this.modalData) {
          obj[`_id`] = this.modalData._id;
          msg = SuccessErrorConst.updatedSuccess;
        }
        this.http.postData(ApiUrl.ADD_SUBSCRIPTION, obj).subscribe(
          () => {
            this.onClose.next();
            this.message.toast('success', msg);
            this.bsModalRef.hide();
          }, () => {
          });
      } else {
          this.showError = true;
      // }
      
        }
      }
    }