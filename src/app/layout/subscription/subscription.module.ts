import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionComponent } from './subscription/subscription.component';
import { AddSubscriptionComponent } from './add-subscription/add-subscription.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';


const routes: Routes = [
  {
      path: '', component: SubscriptionComponent
  }
];


@NgModule({
  declarations: [SubscriptionComponent, AddSubscriptionComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  entryComponents:[
    AddSubscriptionComponent
  ]
})
export class SubscriptionModule { }
