import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitiesComponent } from './cities/cities.component';
import { AddCitiesComponent } from './add-cities/add-cities.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';


const routes: Routes = [
  {
      path: '', component: CitiesComponent
  }
];

@NgModule({
  declarations: [CitiesComponent, AddCitiesComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  entryComponents:[
    AddCitiesComponent
  ]
})
export class CitiesModule { }
