import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// components
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent, children: [
            {
                path: 'tutorials',
                loadChildren: './tutorials/tutorials.module#TutorialsModule',
                data: {breadcrumb: 'Tutorials', title: 'tutorial'}
            },
            {
                path: 'categories',
                loadChildren: './categories/categories.module#CategoriesModule',
                data: {breadcrumb: 'Category', title: 'Category'}
            },
            {
                path: 'subscription',
                loadChildren: './subscription/subscription.module#SubscriptionModule',
                data: {breadcrumb: 'Subscription', title: 'Subscription'}
            },
            {
                path: 'language',
                loadChildren: './language/language.module#LanguageModule',
                data: {breadcrumb: 'Language', title: 'Language'}
            },
            {
                path: 'profileinfo',
                loadChildren: './profileinfo/profileinfo.module#ProfileinfoModule',
                data: {breadcrumb:'Profile Info', title: 'profile Info'}
            },
            {
                path: 'question',
                loadChildren: './question/question.module#QuestionModule',
                data: {breadcrumb:'Questions', title: 'Questions'}
            },
            {
                path: 'answers',
                loadChildren: './answers/answers.module#AnswersModule',
                data: {breadcrumb:'Answers', title: 'Answers'}
            },
            {
                path: 'cities',
                loadChildren: './cities/cities.module#CitiesModule',
                data: {breadcrumb:'Cities', title: 'Cities'}
            },
            {
                path: 'studio',
                loadChildren: './studio/studio.module#StudioModule',
                data: {breadcrumb:'Studio', title: 'studio'}
            },
            {
                path: 'about',
                loadChildren: './about/about.module#AboutModule',
                data: {breadcrumb:'About Us', title: 'about'}
            },
            {
                path: 'users',
                loadChildren: './users/users.module#UsersModule',
                data: {breadcrumb: 'Users', title: 'Users'}
            },
            {
                path: 'videos',
                loadChildren: './videos/videos.module#VideosModule',
                data: {breadcrumb: 'Videos', title: 'Videos'}
            },
            {
                path: 'plans',
                loadChildren: './plans/plans.module#PlansModule',
                data: {breadcrumb: 'Plans', title: 'Plans'}
            },
            {
                path: 'sessionlist',
                loadChildren: './sessionlist/sessionlist.module#SessionlistModule',
                data: {breadcrumb: 'Session', title: 'Session'}
            },
            {
                path: 'article',
                loadChildren: './article/article.module#ArticleModule',
                data: {breadcrumb: 'Article', title: 'article'}
            },
            {
                path: 'policy',
                loadChildren: './policy/policy.module#PolicyModule',
                data: {breadcrumb: 'Privacy Policy', title: 'policy'}
            },
            {
                path: 'terms',
                loadChildren: './terms/terms.module#TermsModule',
                data: {breadcrumb: 'Term And Conditions', title: 'Terms'}
            },
            {
                path: 'contact',
                loadChildren: './contact/contact.module#ContactModule',
                data: {breadcrumb: 'Contact', title: 'About Contact'}
            },
            {
                path: 'membership',
                loadChildren: './membership/membership.module#MembershipModule',
                data: {breadcrumb: 'MemberShip Plan', title: 'MemberShip Plan'}
            },
            {
                path: 'query',
                loadChildren: './query/query.module#QueryModule',
                data: {breadcrumb: 'Queries', title: 'Queries'}
            },



        ]
    }
];

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class LayoutRoutingModule {
}
