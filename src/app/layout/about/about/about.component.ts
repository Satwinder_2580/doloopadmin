import { Component, OnInit } from '@angular/core';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { MessageService } from 'src/app/services/message/message.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  id: any = '';
  update = true;
  text: any;

  constructor(
    private http: HttpService, private message: MessageService
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.http.getData(ApiUrl.ABOUT_LISTING)
      .subscribe(response => {
        if ( response.data.length) {
          this.text = response.data[0].text;
          this.id = response.data._id;
        }
      }, error => {
      });
  }

  submitForm() {
    console.log(this.id)
    let obj = {
      text: this.text,
    }
    if (this.id) {
      obj['_id'] = this.id;
    }
    this.http.postData(ApiUrl.ADD_ABOUT, obj)
      .subscribe(response => {
        this.message.toast('success', SuccessErrorConst.updatedSuccess);
      }, error => {
      });
  }

}
