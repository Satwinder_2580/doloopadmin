import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about/about.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { TrimValueAccessorModule } from 'ng-trim-value-accessor';
import { AngularEditorModule } from '@kolkov/angular-editor';

const routes: Routes = [
  {
    path: '', component:AboutComponent
  }
];

@NgModule({
  declarations: [AboutComponent],
  imports: [
    CommonModule,
     RouterModule.forChild(routes),
    FormsModule,
    TrimValueAccessorModule,
    AngularEditorModule
  ]
})
export class AboutModule { }
