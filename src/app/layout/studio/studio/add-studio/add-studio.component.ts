import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { MessageService } from "src/app/services/message/message.service";
import { ApiUrl } from "src/app/core/apiUrl";
import { HttpService } from "src/app/services/http/http.service";
import { Subject } from "rxjs";
import { BsModalRef } from "ngx-bootstrap/modal";
import { SuccessErrorConst } from "src/app/core/successErrorConst";
import { UtilService } from "../../../../services/util/util.service";

@Component({
  selector: "app-add-studio",
  templateUrl: "./add-studio.component.html",
})
export class AddStudioComponent implements OnInit {
  showError = false;
  form: FormGroup;
  public onClose: Subject<{}> = new Subject();
  modalData: any;
  

  constructor(
    private fb: FormBuilder,
    private message: MessageService,
    private http: HttpService,
    public bsModalRef: BsModalRef,
    public util: UtilService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      // name: ['', Validators.required],
      image: ['', Validators.required],
     
    });

    if (this.modalData) {
      this.patchData(this.modalData);
    } else {
      // this.createItem("longDiscription");
      // this.addItem("longDiscription");
    }
  }

  // createItem(type): FormGroup {
  //   switch (type) {
  //     case "longDiscription":
  //       return this.fb.group({
  //         longDiscription: ['', Validators.required],
  //       });
  //   }
  // }

  // addItem(type): void {
  //   switch (type) {
  //     case "longDiscription":
  //       this.longDiscription.push(this.createItem(type));
  //       break;
  //   }
  // }

  // get longDiscription(): FormArray {
  //   return this.form.get("longDiscription") as FormArray;
  // }

  patchData(data) {
    console.log(data)
    this.form.patchValue({
      // name: data.name,
      // shortDiscription: data.shortDiscription,
      image:data.image,
    });

  //   const longDiscription = [];
  //   data.longDiscription.map((val) => {
  //     this.addItem("longDiscription");
  //     longDiscription.push({
  //       longDiscription: val.longDiscription,
  //     });
  //   });
  //   this.form.patchValue({
  //     longDiscription,
  //   });
  // }
  // getValue(arr,key){
  //   console.log(arr,'opoopo')
  //   let temp=[];
  //   arr.forEach(element => {
  //       temp.push(element[key])
  //   });
    
  //   return temp;
    
    }
  formSubmit() {
    if (this.form.valid) {
      const obj = JSON.parse(JSON.stringify(this.form.value));
      // if (this.image) {
      //   obj.image = (this.image);
      // } else {
      //   this.message.toast("error", SuccessErrorConst.noImageAdded);
      //   return;
      // }
      // obj.longDiscription = this.getValue(this.form.value.longDiscription,'longDiscription');

      let msg = SuccessErrorConst.addedSuccess;
      if (this.modalData) {
        obj[`_id`] = this.modalData._id;
        msg = SuccessErrorConst.updatedSuccess;
      }
      this.http.postData(ApiUrl.Add_Studio, obj).subscribe(
        () => {
          this.onClose.next();
          this.message.toast('success', msg);
          this.bsModalRef.hide();
        },
        () => {}
      );
    } else {
      this.showError = true;
    }
  }
  selectImage(event: any) {
    if (event.target.files && event.target.files[0]) {
        const obj = {
            image: event.target.files[0]
        };
        this.uploadImage(obj);
    }
}

uploadImage(obj) {
    this.http.uploadImageService(ApiUrl.upload_attachment, obj, true).subscribe(response => {
        this.form.controls.image.setValue(this.util.setImagePath(response.data));
        document.getElementById('image')[`value`] = '';
    }, () => {
        document.getElementById('image')[`value`] = '';
    });
}

removeImage() {
    this.form.controls[`image`].setValue('');
}

}
