import { Component, OnInit } from '@angular/core';
import { PaginationControls } from 'src/app/shared/models/pagination-model';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { UtilService } from 'src/app/services/util/util.service';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { MessageService } from 'src/app/services/message/message.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import {AddStudioComponent} from '../studio/add-studio/add-studio.component'
@Component({
  selector: 'app-studio',
  templateUrl: './studio.component.html',
})
export class StudioComponent implements OnInit {

  allData: any = [];
  pagination = new PaginationControls();

  search='';
  constructor(private http: HttpService, private message: MessageService, public util: UtilService,
              private modalService: BsModalService) {
  }

  ngOnInit() {
    this.getData();
}
getData() {
  const obj: any = {
      pageNumber: (this.pagination.page - 1) * 10
  };
  this.http.getData(ApiUrl.studioListing, obj).subscribe(res => {
      this.allData = res.data.dataList;
      this.pagination.count = res.data.totalCount;
  }, () => {
  });
}
blockUnblock(data) {
  this.message.confirm(`${data.isBlocked ? 'unblock' : 'block'} this ${this.util.title}`).then(result => {
      if (result.value) {
          const obj: any = {
              id: data._id,
              isBlocked: !data.isBlocked
          };
          this.http.putData(ApiUrl.MANAGE_STUDIO, obj).subscribe(() => {
              this.util.checkBlockUnblock(data);
          }, () => {
          });
      }
  });
}

deleteData(data) {
  this.message.confirm(`delete this ${this.util.title}`).then(result => {
      if (result.value) {
          const obj: any = {
              id: data._id,
              isDeleted: true
          };
          this.http.putData(ApiUrl.MANAGE_STUDIO, obj).subscribe(() => {
              this.message.toast('success', SuccessErrorConst.deleteSuccess);
              this.getData();
          }, () => {
          });
      }
  });

}

addEditModalOpen(data?: any) {
  const modalRef = this.modalService.show(AddStudioComponent, {
      initialState: {modalData: data}, backdrop: 'static', keyboard: false, class: 'modal-md'
  });
  modalRef.content.onClose.subscribe(result => {
      this.getData();
  });
}

}