import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudioComponent } from './studio/studio.component';
import { AddStudioComponent } from './studio/add-studio/add-studio.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
      path: '', component: StudioComponent
  }
];

@NgModule({
  declarations: [StudioComponent, AddStudioComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
],
entryComponents:[
  AddStudioComponent
]
})
export class StudioModule { }
