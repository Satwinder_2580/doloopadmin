import { Component, OnInit } from '@angular/core';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { SuccessErrorConst } from 'src/app/core/successErrorConst';
import { MessageService } from 'src/app/services/message/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.scss']
})
export class PolicyComponent implements OnInit {

  text = '';
  type = '';
  update = false;
  allData: any = false;
  policy ='';

  constructor(private http: HttpService, private message: MessageService, private router: Router) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.http.getData(ApiUrl.PolicyTermListing, {}).subscribe(res => {
            if (res.data.length) {
                 this.update = true;
                this.policy = res.data[0].policy;
            } else {
                this.allData = undefined;
            }
        }, () => {
        });
    }
    submitForm() {
          const obj: any = {};
          console.log(obj)
          if (this.update) {
              obj._id = this.allData._id;
          }
          obj.policy=this.policy
          this.http.postData(ApiUrl.Add_policyTerm, obj).subscribe(() => {
              this.message.toast('success', SuccessErrorConst.updatedSuccess);
          }, () => {
          });
      } 
  }


