import { Component, OnInit } from '@angular/core';
import { PaginationControls } from 'src/app/shared/models/pagination-model';
import { ApiUrl } from 'src/app/core/apiUrl';
import { HttpService } from 'src/app/services/http/http.service';
import { UtilService } from 'src/app/services/util/util.service';
import { MessageService } from 'src/app/services/message/message.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {

    allData: any = [];
    pagination = new PaginationControls();
    search: any;

    constructor(private http: HttpService, private message: MessageService, public util: UtilService) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        const obj: any = {
            pageNumber: (this.pagination.page - 1) * 10
          };
          if (this.search) {
            obj.search = this.search;
          } else {
            delete obj.search;
          }
        this.http.getData(ApiUrl.USER_LIST, obj).subscribe(res => {
            this.allData = res.data.dataList;
            this.pagination.count = res.data.totalCount;
        });
    }
  

    blockUnblock(data) {
        this.message.confirm(`${data.isBlocked ? 'unblock' : 'block'} this ${this.util.title}`).then(result => {
            if (result.value) {
                const obj: any = {
                    _id: data._id,
                    isBlocked: !data.isBlocked
                };
                this.http.putData(ApiUrl.MANAGE_USER + '?_id=' + data._id + '&isBlocked=' + !data.isBlocked, obj).subscribe(() => {
                    this.util.checkBlockUnblock(data);
                }, () => {
                });
            }
        });
    }

}
