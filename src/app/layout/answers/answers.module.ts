import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnswersComponent } from './answers/answers.component';
import { AddAnswersComponent } from './add-answers/add-answers.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';


const routes: Routes = [
  {
      path: '', component: AnswersComponent
  }
];

@NgModule({
  declarations: [AnswersComponent, AddAnswersComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  entryComponents:[
    AddAnswersComponent
  ]
})
export class AnswersModule { }
