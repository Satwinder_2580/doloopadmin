import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesComponent } from './categories/categories.component';
import { AddCategoriesComponent } from './add-categories/add-categories.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';


const routes: Routes = [
  {
      path: '', component: CategoriesComponent
  }
];


@NgModule({
  declarations: [CategoriesComponent, AddCategoriesComponent],
  imports: [
    CommonModule,    
    SharedModule,
    RouterModule.forChild(routes)
],
entryComponents: [
  AddCategoriesComponent
]
})
export class CategoriesModule { }
