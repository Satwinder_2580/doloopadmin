export class PaginationControls {
    page = 1;
    perPage = 10;
    skip = 0;
    pageIndex = 0;
    count = 0;
    limitValue: any = [10, 30, 50];
    allData: any = [];
}
